import { Reducer } from 'redux';
import { OPERATIONS, OPERATIONS_WITHOUT_TWO_ARGS } from 'state/actions/operations';
import { AppAction } from '../AppAction';

type Operation = {
  arg0: number;
  arg1: number | undefined;
  result: number;
}

type CalculatorState = {
  stackOperation: Operation[];
  currentOperation: Operation | undefined;
  stack: number[];
  input: string | undefined;
  lastState: string | undefined;
};

const initialState: CalculatorState = {
  stackOperation: [],
  currentOperation: undefined,
  stack: [],
  input: undefined,
  lastState: undefined,
};

export const calculatorReducer: Reducer<CalculatorState, AppAction> = (
  state = initialState,
  action
) => {
  const lastState = JSON.stringify(state);

  switch (action.type) {
    case 'OPERATION':
      const arg0 = action.function !== OPERATIONS.SUM ? state.input ?  parseFloat(state.input) : state.stack.shift() : 0;
      const arg1 = action.function !== OPERATIONS.SUM ? state.stack.shift() : 0;
      
      if (arg0 === undefined) {
        return state;
      }

      if(arg1 === undefined && !OPERATIONS_WITHOUT_TWO_ARGS.includes(action.function)) {
        return {
          ...state,
          stack: state.input ? state.stack : [arg0, ...state.stack],
        }
      }

      switch(action.function) {
        case OPERATIONS.ADDITION:
          const addition_result = arg1! + arg0;
          return {
            ...state,
            stack: [...state.stack, addition_result],
            input: undefined,
            currentOperation: {
              arg0, arg1,
              result: addition_result,
            },
            lastState,
          };
        case OPERATIONS.SUBTRACTION:
          const substaction_result = arg1! - arg0;
          return {
            ...state,
            stack: [...state.stack, substaction_result],
            input: undefined,
            currentOperation: {
              arg0, arg1,
              result: substaction_result,
            },
            lastState,
          };
        case OPERATIONS.MULTIPLICATION:
          const multiplication_result = arg1! * arg0;
          return {
            ...state,
            stack: [...state.stack, multiplication_result],
            input: undefined,
            currentOperation: {
              arg0, arg1,
              result: multiplication_result,
            },
            lastState,
          };
        case OPERATIONS.DIVISION:
          const division_result = arg1! / arg0;
          return {
            ...state,
            stack: [...state.stack, division_result],
            input: undefined,
            currentOperation: {
              arg0, arg1,
              result: division_result,
            },
            lastState,
          };
        case OPERATIONS.SQUARE_ROOT:
          const square_root_result = Math.sqrt(arg0);
          return {
            ...state,
            stack: [...state.stack, square_root_result],
            input: undefined,
            currentOperation: {
              arg0, arg1: undefined,
              result: square_root_result,
            },
            lastState,
          };
        case OPERATIONS.SUM:
          const initialValue = 0;
          const sum_result = state.stack.reduce(
            (previousValue, currentValue) => previousValue + currentValue,
            initialValue
          );
          return {
            ...state,
            stack: [sum_result],
            input: undefined,
            currentOperation: {
              arg0, arg1,
              result: sum_result,
            },
            lastState,
          };
        case OPERATIONS.INTRO:
          return {
            ...state,
            stack: arg1 ? [arg0, arg1, ...state.stack] : [arg0, ...state.stack],
            input: undefined,
            lastState,
          }
        default: return state;
      }
    case 'INPUT':
      const newInput = (state.input?.toString() || '' ) + action.value.toString();
     return {
        ...state,
        input: newInput,
        lastState,
     }
    case 'POINT':
      const inputWithPoint = (state.input?.toString() || '') + '.';
      return {
        ...state,
        input: inputWithPoint,
        lastState,
      }
    case 'UNDO':
      return state.lastState ? JSON.parse(state.lastState) : state;
    default:
      return state;
  }
};
