import { combineReducers } from 'redux';

import { calculatorReducer } from './calculatorReducer';
import { higherOrderReducer } from './higherOrderReducer';

export const rootReducer = higherOrderReducer(
  combineReducers({
    calculator: calculatorReducer,
  })
);
