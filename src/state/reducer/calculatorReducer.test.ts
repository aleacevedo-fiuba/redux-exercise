import { changeSample } from 'state/actions';
import { calculatorReducer } from './calculatorReducer';

describe('sampleReducer', function () {
  it('initial state', () => {
    const initialState = calculatorReducer(undefined, { type: undefined as any });
    expect(initialState).toEqual({
      message: 'Hello world',
    });
  });

  it('replaces message on sample action', () => {
    const next = calculatorReducer({ message: 'WRONG MESSAGE' }, changeSample());
    expect(next).toEqual({ message: 'Sample' });
  });
});
