export enum OPERATIONS {
  ADDITION = 'ADDITION',
  SUBTRACTION = 'SUBTRACTION',
  MULTIPLICATION = 'MULTIPLICATION',
  DIVISION = 'DIVISION',
  SQUARE_ROOT = 'SQUARE_ROOT',
  SUM = 'SUM',
  INTRO = 'INTRO',
}

export const OPERATIONS_WITHOUT_TWO_ARGS = [OPERATIONS.SQUARE_ROOT, OPERATIONS.INTRO];

type OperationActions = {
  type: 'OPERATION'
  function: OPERATIONS
}

type StackActions = {
  type: 'INPUT';
  value: number;
}

type PointActions = {
  type: 'POINT';
}

type UndoActions = {
  type: 'UNDO';
}

type UndefAction = {
  type: undefined;
}

type CalculatorActions = OperationActions | StackActions | PointActions | UndoActions | UndefAction;

export const executeOperation = (operation: OPERATIONS): CalculatorActions => ({
  type: 'OPERATION',
  function: operation,
});

export const input = (value: number): CalculatorActions => ({
  type: 'INPUT',
  value,
});

export const point = (): CalculatorActions => ({
  type: 'POINT',
});

export const undo = (): CalculatorActions => ({
  type: 'UNDO'
});