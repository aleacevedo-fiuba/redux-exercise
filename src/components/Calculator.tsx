import { useDispatch, useSelector } from 'react-redux';

import { executeOperation, input, point, OPERATIONS, undo } from 'state/actions/operations';
import { selectCurrentNumber } from 'state/selectors/selectCurrentNumber';
import { selectCurrentStack } from 'state/selectors/selectCurrentStack';

import styles from './Calculator.module.css';

const renderStackItem = (value: number, index: number) => {
  return <div key={index}>{value}</div>;
};

export const Calculator = () => {
  const currentNumber = useSelector(selectCurrentNumber);
  const stack = useSelector(selectCurrentStack);

  const dispatch = useDispatch();
  const onClickNumber = (n: number) => {
    const action = input(n);
    dispatch(action);
  };
  const onClickDot = () => {
    const action = point();
    dispatch(action);
  };
  const onClick = (operation: OPERATIONS) => {
    dispatch(executeOperation(operation));
  };
  const onClickUndo = () => {
    dispatch(undo());
  };

  return (
    <div className={styles.main}>
      <div className={styles.display}>{currentNumber}</div>
      <div className={styles.numberKeyContainer}>
        {[...Array(9).keys()].map((i) => (
          <button key={i} onClick={() => onClickNumber(i + 1)}>
            {i + 1}
          </button>
        ))}
        <button className={styles.zeroNumber} onClick={() => onClickNumber(0)}>
          0
        </button>
        <button onClick={() => onClickDot()}>.</button>
      </div>
      <div className={styles.opKeyContainer}>
        <button onClick={() => onClick(OPERATIONS.ADDITION)}>+</button>
        <button onClick={() => onClick(OPERATIONS.SUBTRACTION)}>-</button>
        <button onClick={() => onClick(OPERATIONS.MULTIPLICATION)}>x</button>
        <button onClick={() => onClick(OPERATIONS.DIVISION)}>/</button>
        <button onClick={() => onClick(OPERATIONS.SQUARE_ROOT)}>√</button>
        <button onClick={() => onClick(OPERATIONS.SUM)}>Σ</button>
        <button onClick={() => onClickUndo()}>Undo</button>
        <button onClick={() => onClick(OPERATIONS.INTRO)}>Intro</button>
      </div>
      <div className={styles.stack}>{stack.map(renderStackItem)}</div>
    </div>
  );
};
